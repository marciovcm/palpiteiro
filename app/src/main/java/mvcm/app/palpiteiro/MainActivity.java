package mvcm.app.palpiteiro;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import mvcm.app.palpiteiro.asynctask.LotoChartTask;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Palpiteiro";

    private static final int PICKFILE_REQUEST_CODE = 1;

    private Menu mMenu;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;
    private CheckAppPermission checkPermission;

    private boolean isLotofacil = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setElevation(0);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = mSharedPreferences.edit();

        checkPermission = new CheckAppPermission(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

         checkPermission.askMemoryPermission();

        if(mSharedPreferences.getString("sorteio", "lotofacil").equals("lotofacil")){
            showFragment(FragmentLotofacil.newInstance());
            isLotofacil = true;
        }else{
            showFragment(FragmentMegasena.newInstance());
            isLotofacil = false;
        }
    }

    private void showFragment(Fragment fragment){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container, fragment, "FragmentLotofacil").commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        mMenu = menu;
        toggleOptionMenu(isLotofacil);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_add_refresh:
                if(isLotofacil){
                    LotoChartTask task = new LotoChartTask(this);
                    task.execute("LotoChartTask");
                }else{
//                    MegaChartTask task = new MegaChartTask();
//                    task.execute("MegaChartTask");
                }
                break;
            case R.id.menu_add_result:
                intent = null;
                if(isLotofacil){
                    intent = new Intent(this, LotoActivity.class);
                }else{
                    //intent = new Intent(this, MegaActivity.class);
                }
                startActivity(intent);
                break;
            case R.id.menu_import_file:
                importExternalFile();
                break;
            case R.id.menu_export_file:
                break;
            case R.id.menu_history:
                intent = null;
                intent = new Intent(this, HistoryActivity.class);
                if(isLotofacil){
                    intent.putExtra("game", "lotofacil");
                }else{
                    //intent.putExtra("game", "megasena");
                }
                startActivity(intent);
                break;
            case R.id.menu_toggle_lotofacil:
                showFragment(FragmentLotofacil.newInstance());
                editor.putString("sorteio", "lotofacil").commit();
                isLotofacil = true;
                break;
            case R.id.menu_toggle_megasena:
                showFragment(FragmentMegasena.newInstance());
                editor.putString("sorteio", "megasena").commit();
                isLotofacil = false;
                break;
        }

        toggleOptionMenu(isLotofacil);

        return super.onOptionsItemSelected(item);
    }

    public void importExternalFile(){
        Intent intent = new Intent();
        intent.setAction("com.sec.android.app.myfiles.PICK_DATA");
        intent.putExtra("CONTENT_TYPE", "*/*");
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    public void toggleOptionMenu(boolean isLotofacil){
        mMenu.findItem(R.id.menu_toggle_lotofacil).setVisible(!isLotofacil);
        mMenu.findItem(R.id.menu_toggle_megasena).setVisible(isLotofacil);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult - requestCode: " + Integer.toString(requestCode));

        Uri mUri;

        if(requestCode == PICKFILE_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                mUri = data.getData();
                Log.d(TAG,"URI selected file = "+ mUri.toString());

                if(isLotofacil){
                    FragmentLotofacil.callLotoTask(mUri, this);
                }else{
//                    FragmentMegasena.callMegaTask(mUri, this);
                }
            }
        }
    }
}

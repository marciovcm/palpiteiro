package mvcm.app.palpiteiro;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import mvcm.app.palpiteiro.database.DbHelper;
import mvcm.app.palpiteiro.database.LotoTable;


public class HistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        getSupportActionBar().setTitle(getResources().getString(R.string.str_main_menu_history));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String str_game = getIntent().getStringExtra("game");

        DbHelper dbHelper = DbHelper.getInstance(this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = null;
        ListView listView = (ListView) findViewById(R.id.history_listview);
        HistoryAdapter adapter = null;

        if(str_game.equals("lotofacil")){
            cursor = database.rawQuery(LotoTable.SELECT_ALL, null);
        }

        adapter = new HistoryAdapter(this, cursor, false);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

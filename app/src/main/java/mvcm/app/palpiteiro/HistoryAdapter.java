package mvcm.app.palpiteiro;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import mvcm.app.palpiteiro.database.LotoTable;


public class HistoryAdapter extends CursorAdapter {

    public HistoryAdapter(Context context, Cursor c, boolean autoRequery){
        super(context, c, autoRequery);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();

        String str_number = cursor.getString(LotoTable.COLUMN_LOTO_DRAW_IDX);
        String str_result = cursor.getString(LotoTable.COLUMN_LOTO_RESULT_IDX);

        holder.number.setText(str_number);
        holder.results.setText(str_result);

        view.setId(LotoTable.COLUMN_LOTO_ID_IDX);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.history_row, parent, false);

        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);

        return view;
    }

    public static class ViewHolder{
        TextView number;
        TextView results;

        public ViewHolder(View view){
            number = (TextView) view.findViewById(R.id.history_draw_number);
            results = (TextView) view.findViewById(R.id.history_draw_result);
        }
    }
}

package mvcm.app.palpiteiro;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AlertDialog;

class CheckAppPermission {
    private Activity mActivity;

    CheckAppPermission (Activity activity){
        mActivity = activity;
    }

    @TargetApi(Build.VERSION_CODES.M)
    boolean askMemoryPermission()
    {
        if (hasMemoryPermission())
            return true;
        else{
            if (mActivity.shouldShowRequestPermissionRationale("android.permission.READ_EXTERNAL_STORAGE")) {
                AlertDialog permissionDialog;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mActivity);

                dialogBuilder.setTitle(R.string.title_dialog_memory)
                        .setMessage(R.string.dialog_memory_perm_message)
                        .setPositiveButton(R.string.str_dialog_btn_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String[] perStr = {"android.permission.READ_EXTERNAL_STORAGE"};
                                mActivity.requestPermissions(perStr, 0);
                            }
                        })
                        .setNegativeButton(R.string.str_dialog_btn_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                permissionDialog = dialogBuilder.create();
                permissionDialog.show();
            } else {
                String[] perStr = {"android.permission.READ_EXTERNAL_STORAGE"};
                mActivity.requestPermissions(perStr, 0);
            }
            return false;
        }
    }

    boolean hasMemoryPermission(){
        return (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                mActivity.checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED);
    }
}

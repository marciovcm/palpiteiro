package mvcm.app.palpiteiro.asynctask;


import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mvcm.app.palpiteiro.R;
import mvcm.app.palpiteiro.database.DbHelper;
import mvcm.app.palpiteiro.database.LotoTable;
import mvcm.app.palpiteiro.model.Lotofacil;

public class LotoChartTask extends AsyncTask<String, Void, String>{

    DbHelper dbHelper;
    SQLiteDatabase database;
    Cursor cursor;
    ProgressDialog dialog;
    Context context;

    public LotoChartTask(Context context){
        this.context = context;
        dbHelper = DbHelper.getInstance(this.context);
        database = dbHelper.getWritableDatabase();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(context.getResources().getString(R.string.str_loto_dialog_calculating_charts));
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        Lotofacil lotofacil = new Lotofacil();
        String task_result = "calculating_failed";
        String str = null;
        String[] str_array;
        final Map<String, Integer> map = new HashMap<>();

        for(int i=1; i<=25; i++){
            str = String.format("%02d", i);
            map.put(str, 0);
        }

        cursor = database.rawQuery(LotoTable.SELECT_ALL_RESULTS, null);
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                str = cursor.getString(0); //parameter ZERO because there is only one column added to cursor
                str_array = str.split("-");
                for (int i=0; i<15; i++){
                    if(map.get(str_array[i]) == 0){
                        map.put(str_array[i], 1);
                    }else{
                        map.put(str_array[i], map.get(str_array[i]) +1);
                    }
                }
            }
            task_result = "calculating_success";
            cursor.close();
        }

        lotofacil.chart_geral = new ArrayList(map.keySet());
        Collections.sort(lotofacil.chart_geral, new Comparator() {
            @Override
            public int compare(Object left, Object right) {
                return map.get(right).compareTo(map.get(left));
            }
        });

        map.clear();

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        for(int i=1; i<=25; i++){
            str = String.format("%02d", i);
            map.put(str, 0);
        }

        String[] whereArgs = new String[]{String.valueOf(year)};
        cursor = database.rawQuery(LotoTable.SELECT_RESULTS_BY_YEAR, whereArgs);
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                str = cursor.getString(0);
                str_array = str.split("-");
                for(int i=0; i<15; i++){
                    if(map.get(str_array[i]) == 0){
                        map.put(str_array[i], 1);
                    }else{
                        map.put(str_array[i], map.get(str_array[i]) +1);
                    }
                }
            }
            task_result = "calculating_success";
            cursor.close();
        }

        lotofacil.chart_year = new ArrayList(map.keySet());
        Collections.sort(lotofacil.chart_year, new Comparator() {
            @Override
            public int compare(Object left, Object right) {
                return map.get(right).compareTo(map.get(left));
            }
        });

        int month = c.get(Calendar.MONTH) + 1;

        map.clear();

        for(int i=1; i<=25; i++){
            str = String.format("%02d", i);
            map.put(str, 0);
        }

        whereArgs = new String[]{String.valueOf(year), String.format("%02d", month)};
        cursor = database.rawQuery(LotoTable.SELECT_RESULTS_BY_MONTH, whereArgs);
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                str = cursor.getString(0);
                str_array = str.split("-");
                for(int i=0; i<15; i++){
                    if(map.get(str_array[i]) == 0){
                        map.put(str_array[i], 1);
                    }else{
                        map.put(str_array[i], map.get(str_array[i]) +1);
                    }
                }
            }
            task_result = "calculating_success";
            cursor.close();
        }

        lotofacil.chart_month = new ArrayList(map.keySet());
        Collections.sort(lotofacil.chart_month, new Comparator() {
            @Override
            public int compare(Object left, Object right) {
                return map.get(right).compareTo(map.get(left));
            }
        });

        map.clear();

        return task_result;
    }

    @Override
    protected void onPostExecute(String task_result) {
        super.onPostExecute(task_result);
        dialog.dismiss();

        if(task_result.equals("calculating_success")){
            Toast.makeText(context, R.string.str_loto_toast_chart_success, Toast.LENGTH_SHORT).show();
        }else if(task_result.equals("calculating_failed")){
            Toast.makeText(context, R.string.str_loto_toast_chart_fail, Toast.LENGTH_SHORT).show();
        }
    }

}

package mvcm.app.palpiteiro.asynctask;


import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.File;
import java.util.Scanner;

import mvcm.app.palpiteiro.R;
import mvcm.app.palpiteiro.database.DbHelper;
import mvcm.app.palpiteiro.database.LotoTable;

public class LotoImportTask extends AsyncTask<String, Void, String>{

    DbHelper dbHelper;
    SQLiteDatabase database;
    ProgressDialog dialog;
    Context context;

    String loto_day;
    String loto_month;
    String loto_year;
    String path;

    public LotoImportTask(Context context, String path){
        this.context = context;
        this.path = path;
        dbHelper = DbHelper.getInstance(this.context);
        database = dbHelper.getWritableDatabase();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(context.getResources().getString(R.string.str_loto_dialog_importing_results));
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        ContentValues values = new ContentValues();
        Scanner input;
        String str = null;
        String task_result = "importing_failed";

        database.execSQL(LotoTable.DELETE_CONTACTS_TABLE);
        database.execSQL((LotoTable.CREATE_LOTO_TABLE));

        try {
            File file = new File(path);
            if(file != null){
                input = new Scanner(file);
                while (input.hasNextLine()){
                    values.put(LotoTable.COLUMN_LOTO_DRAW, input.next());

                    getDate(input.next());
                    values.put(LotoTable.COLUMN_LOTO_DAY, loto_day);
                    values.put(LotoTable.COLUMN_LOTO_MONTH, loto_month);
                    values.put(LotoTable.COLUMN_LOTO_YEAR, loto_year);

                    for(int i=0; i<15; i++){
                        if(str == null){
                            str = input.next() + "-";
                        }else{
                            str += input.next() + "-";
                        }
                    }
                    values.put(LotoTable.COLUMN_LOTO_RESULT, str.substring(0, (str.length() - 1)));

                    database.insert(LotoTable.TABLE_LOTOFACIL, null, values);
                    values.clear();
                    str = null;
                }

                input.close();
                task_result = "importing_success";
            }
        }catch (Exception e){
            e.printStackTrace();
            task_result = "importing_failed";
        }

        return task_result;
    }

    @Override
    protected void onPostExecute(String task_result) {
        super.onPostExecute(task_result);
        dialog.dismiss();

        if(task_result.equals("importing_success")){
            Toast.makeText(context, R.string.str_loto_toast_import_success, Toast.LENGTH_SHORT).show();
        }else if(task_result.equals("importing_failed")){
            Toast.makeText(context, R.string.str_loto_toast_import_fail, Toast.LENGTH_SHORT).show();
        }
    }

    public void getDate(String str){
        String[] array = str.split("/");
        loto_day = array[0];
        loto_month = array[1];
        loto_year = array[2];
    }
}

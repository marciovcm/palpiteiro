package mvcm.app.palpiteiro;


import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class FragmentMegasena extends Fragment{

    public static FragmentMegasena newInstance(){
        return new FragmentMegasena();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_megasena, container, false);

        BottomNavigationView navigation = (BottomNavigationView) view.findViewById(R.id.mega_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        return view;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.mega_navigation_geral:
                    return true;
                case R.id.mega_navigation_yearly:
                    return true;
                case R.id.mega_navigation_monthly:
                    return true;
            }
            return false;
        }

    };

}

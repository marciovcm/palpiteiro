package mvcm.app.palpiteiro.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper{

    private static DbHelper instance;

    private static final String DATABASE_NAME = "palpiteiro.db";
    private static final int DATABASE_VERSION = 1;

    // Constructor should be private to prevent direct instantiation.
    // make call to static method "getInstance()" instead.
    private DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DbHelper getInstance(Context context){
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if(instance == null){
            instance = new DbHelper(context.getApplicationContext());
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LotoTable.CREATE_LOTO_TABLE);
        db.execSQL(MegaTable.CREATE_MEGA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

package mvcm.app.palpiteiro.database;


public class MegaTable {

    // Database table and columns
    public static final String TABLE_MEGASENA = "megasena";
    public static final String COLUMN_MEGA_ID = "_id";
    public static final String COLUMN_MEGA_DRAW = "draw_num";
    public static final String COLUMN_MEGA_RESULT = "result";
    public static final String COLUMN_MEGA_DAY = "day";
    public static final String COLUMN_MEGA_MONTH = "month";
    public static final String COLUMN_MEGA_YEAR = "year";


    public static final int COLUMN_MEGA_ID_IDX = 0;
    public static final int COLUMN_MEGA_DRAW_IDX = 1;
    public static final int COLUMN_MEGA_RESULT_IDX = 2;
    public static final int COLUMN_MEGA_DAY_IDX = 3;
    public static final int COLUMN_MEGA_MONTH_IDX = 4;
    public static final int COLUMN_MEGA_YEAR_IDX = 5;


    // Database creation sql statement
    public static final String CREATE_MEGA_TABLE = "CREATE TABLE " + TABLE_MEGASENA
            + "( "
            + COLUMN_MEGA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_MEGA_DRAW + " TEXT NOT NULL, "
            + COLUMN_MEGA_RESULT + " TEXT NOT NULL, "
            + COLUMN_MEGA_DAY + " TEXT NOT NULL, "
            + COLUMN_MEGA_MONTH + " TEXT NOT NULL, "
            + COLUMN_MEGA_YEAR + " TEXT NOT NULL"
            + " )";

}

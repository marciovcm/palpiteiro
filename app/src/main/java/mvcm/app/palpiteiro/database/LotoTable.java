package mvcm.app.palpiteiro.database;


public class LotoTable {

    // Database table and columns
    public static final String TABLE_LOTOFACIL = "lotofacil";
    public static final String COLUMN_LOTO_ID = "_id";
    public static final String COLUMN_LOTO_DRAW = "draw_num";
    public static final String COLUMN_LOTO_RESULT = "result";
    public static final String COLUMN_LOTO_DAY = "day";
    public static final String COLUMN_LOTO_MONTH = "month";
    public static final String COLUMN_LOTO_YEAR = "year";


    public static final int COLUMN_LOTO_ID_IDX = 0;
    public static final int COLUMN_LOTO_DRAW_IDX = 1;
    public static final int COLUMN_LOTO_RESULT_IDX = 2;
    public static final int COLUMN_LOTO_DAY_IDX = 3;
    public static final int COLUMN_LOTO_MONTH_IDX = 4;
    public static final int COLUMN_LOTO_YEAR_IDX = 5;


    // Database creation sql statement
    public static final String CREATE_LOTO_TABLE = "CREATE TABLE " + TABLE_LOTOFACIL
            + "( "
            + COLUMN_LOTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_LOTO_DRAW + " TEXT NOT NULL, "
            + COLUMN_LOTO_RESULT + " TEXT NOT NULL, "
            + COLUMN_LOTO_DAY + " TEXT NOT NULL, "
            + COLUMN_LOTO_MONTH + " TEXT NOT NULL, "
            + COLUMN_LOTO_YEAR + " TEXT NOT NULL"
            + " )";

    public static final String DELETE_CONTACTS_TABLE = "DROP TABLE IF EXISTS " + TABLE_LOTOFACIL;

    public static final String SELECT_ALL_RESULTS = "SELECT " + COLUMN_LOTO_RESULT + " FROM " + TABLE_LOTOFACIL;

    public static final String SELECT_ALL = "SELECT * FROM " + TABLE_LOTOFACIL
            + " ORDER BY " + COLUMN_LOTO_DRAW + " DESC";

    public static final String SELECT_RESULTS_BY_YEAR = "SELECT " + COLUMN_LOTO_RESULT + " FROM " + TABLE_LOTOFACIL
            + " WHERE " + COLUMN_LOTO_YEAR + " = ?";

    public static final String SELECT_RESULTS_BY_MONTH = "SELECT " + COLUMN_LOTO_RESULT + " FROM " + TABLE_LOTOFACIL
            + " WHERE " + COLUMN_LOTO_YEAR + " = ? AND " + COLUMN_LOTO_MONTH + " = ?";

}

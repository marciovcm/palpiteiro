package mvcm.app.palpiteiro;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Handler;

import mvcm.app.palpiteiro.asynctask.LotoImportTask;
import mvcm.app.palpiteiro.database.DbHelper;
import mvcm.app.palpiteiro.database.LotoTable;
import mvcm.app.palpiteiro.model.Lotofacil;

public class FragmentLotofacil extends Fragment{

    private static final String TAG = "Palpiteiro";

    TextView txt01, txt02, txt03, txt04, txt05, txt06, txt07, txt08, txt09, txt10
            ,txt11, txt12, txt13, txt14, txt15, txt16, txt17, txt18, txt19, txt20
            ,txt21, txt22, txt23, txt24, txt25;

    BottomNavigationView navigation;

    public static FragmentLotofacil newInstance(){
        return new FragmentLotofacil();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lotofacil, container, false);

        txt01 = (TextView) view.findViewById(R.id.loto_key1);
        txt02 = (TextView) view.findViewById(R.id.loto_key2);
        txt03 = (TextView) view.findViewById(R.id.loto_key3);
        txt04 = (TextView) view.findViewById(R.id.loto_key4);
        txt05 = (TextView) view.findViewById(R.id.loto_key5);
        txt06 = (TextView) view.findViewById(R.id.loto_key6);
        txt07 = (TextView) view.findViewById(R.id.loto_key7);
        txt08 = (TextView) view.findViewById(R.id.loto_key8);
        txt09 = (TextView) view.findViewById(R.id.loto_key9);
        txt10 = (TextView) view.findViewById(R.id.loto_key10);
        txt11 = (TextView) view.findViewById(R.id.loto_key11);
        txt12 = (TextView) view.findViewById(R.id.loto_key12);
        txt13 = (TextView) view.findViewById(R.id.loto_key13);
        txt14 = (TextView) view.findViewById(R.id.loto_key14);
        txt15 = (TextView) view.findViewById(R.id.loto_key15);
        txt16 = (TextView) view.findViewById(R.id.loto_key16);
        txt17 = (TextView) view.findViewById(R.id.loto_key17);
        txt18 = (TextView) view.findViewById(R.id.loto_key18);
        txt19 = (TextView) view.findViewById(R.id.loto_key19);
        txt20 = (TextView) view.findViewById(R.id.loto_key20);
        txt21 = (TextView) view.findViewById(R.id.loto_key21);
        txt22 = (TextView) view.findViewById(R.id.loto_key22);
        txt23 = (TextView) view.findViewById(R.id.loto_key23);
        txt24 = (TextView) view.findViewById(R.id.loto_key24);
        txt25 = (TextView) view.findViewById(R.id.loto_key25);

        navigation = (BottomNavigationView) view.findViewById(R.id.loto_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        return view;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.loto_navigation_geral:
                    updateView("geral");
                    return true;
                case R.id.loto_navigation_yearly:
                    updateView("year");
                    return true;
                case R.id.loto_navigation_monthly:
                    updateView("month");
                    return true;
            }
            return false;
        }

    };

    public static void callLotoTask(Uri uri, Context context){
        LotoImportTask task = new LotoImportTask(context, uri.getPath());
        task.execute("LotoImportTask");
    }

    public void updateView(String type){
        Lotofacil lotofacil = new Lotofacil();
        List list = null;
        String op;

        txt01.setSelected(false);
        txt02.setSelected(false);
        txt03.setSelected(false);
        txt04.setSelected(false);
        txt05.setSelected(false);
        txt06.setSelected(false);
        txt07.setSelected(false);
        txt08.setSelected(false);
        txt09.setSelected(false);
        txt10.setSelected(false);
        txt11.setSelected(false);
        txt12.setSelected(false);
        txt13.setSelected(false);
        txt14.setSelected(false);
        txt15.setSelected(false);
        txt16.setSelected(false);
        txt17.setSelected(false);
        txt18.setSelected(false);
        txt19.setSelected(false);
        txt20.setSelected(false);
        txt21.setSelected(false);
        txt22.setSelected(false);
        txt23.setSelected(false);
        txt24.setSelected(false);
        txt25.setSelected(false);

        if(type.equals("geral")){
            list = lotofacil.chart_geral;
        }else if(type.equals("year")){
            list = lotofacil.chart_year;
        }else if(type.equals("month")){
            list = lotofacil.chart_month;
        }

        for(int i=0; i<15; i++){
            if(list != null){
                op = list.get(i).toString();
                switch (op){
                    case "01":
                        txt01.setSelected(true);
                        break;
                    case "02":
                        txt02.setSelected(true);
                        break;
                    case "03":
                        txt03.setSelected(true);
                        break;
                    case "04":
                        txt04.setSelected(true);
                        break;
                    case "05":
                        txt05.setSelected(true);
                        break;
                    case "06":
                        txt06.setSelected(true);
                        break;
                    case "07":
                        txt07.setSelected(true);
                        break;
                    case "08":
                        txt08.setSelected(true);
                        break;
                    case "09":
                        txt09.setSelected(true);
                        break;
                    case "10":
                        txt10.setSelected(true);
                        break;
                    case "11":
                        txt11.setSelected(true);
                        break;
                    case "12":
                        txt12.setSelected(true);
                        break;
                    case "13":
                        txt13.setSelected(true);
                        break;
                    case "14":
                        txt14.setSelected(true);
                        break;
                    case "15":
                        txt15.setSelected(true);
                        break;
                    case "16":
                        txt16.setSelected(true);
                        break;
                    case "17":
                        txt17.setSelected(true);
                        break;
                    case "18":
                        txt18.setSelected(true);
                        break;
                    case "19":
                        txt19.setSelected(true);
                        break;
                    case "20":
                        txt20.setSelected(true);
                        break;
                    case "21":
                        txt21.setSelected(true);
                        break;
                    case "22":
                        txt22.setSelected(true);
                        break;
                    case "23":
                        txt23.setSelected(true);
                        break;
                    case "24":
                        txt24.setSelected(true);
                        break;
                    case "25":
                        txt25.setSelected(true);
                        break;
                }
            }
        }

    }

}

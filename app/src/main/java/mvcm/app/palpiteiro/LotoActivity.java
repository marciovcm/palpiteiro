package mvcm.app.palpiteiro;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import mvcm.app.palpiteiro.database.DbHelper;
import mvcm.app.palpiteiro.database.LotoTable;

public class LotoActivity extends AppCompatActivity implements View.OnClickListener{

    EditText draw_number;
    EditText draw_date;
    Button txt01, txt02, txt03, txt04, txt05, txt06, txt07, txt08, txt09, txt10
            ,txt11, txt12, txt13, txt14, txt15, txt16, txt17, txt18, txt19, txt20
            ,txt21, txt22, txt23, txt24, txt25;
    Button button_save;

    ArrayList<String> list = new ArrayList<>();

    DbHelper dbHelper;
    SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lotofacil);

        getSupportActionBar().setTitle(getResources().getString(R.string.str_loto_toolbar_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dbHelper = DbHelper.getInstance(this);
        database = dbHelper.getWritableDatabase();

        draw_number = (EditText) findViewById(R.id.loto_draw_number);
        draw_date = (EditText) findViewById(R.id.loto_draw_date);
        button_save = (Button) findViewById(R.id.button_save_loto_result);
        txt01 = (Button) findViewById(R.id.loto_button1);
        txt02 = (Button) findViewById(R.id.loto_button2);
        txt03 = (Button) findViewById(R.id.loto_button3);
        txt04 = (Button) findViewById(R.id.loto_button4);
        txt05 = (Button) findViewById(R.id.loto_button5);
        txt06 = (Button) findViewById(R.id.loto_button6);
        txt07 = (Button) findViewById(R.id.loto_button7);
        txt08 = (Button) findViewById(R.id.loto_button8);
        txt09 = (Button) findViewById(R.id.loto_button9);
        txt10 = (Button) findViewById(R.id.loto_button10);
        txt11 = (Button) findViewById(R.id.loto_button11);
        txt12 = (Button) findViewById(R.id.loto_button12);
        txt13 = (Button) findViewById(R.id.loto_button13);
        txt14 = (Button) findViewById(R.id.loto_button14);
        txt15 = (Button) findViewById(R.id.loto_button15);
        txt16 = (Button) findViewById(R.id.loto_button16);
        txt17 = (Button) findViewById(R.id.loto_button17);
        txt18 = (Button) findViewById(R.id.loto_button18);
        txt19 = (Button) findViewById(R.id.loto_button19);
        txt20 = (Button) findViewById(R.id.loto_button20);
        txt21 = (Button) findViewById(R.id.loto_button21);
        txt22 = (Button) findViewById(R.id.loto_button22);
        txt23 = (Button) findViewById(R.id.loto_button23);
        txt24 = (Button) findViewById(R.id.loto_button24);
        txt25 = (Button) findViewById(R.id.loto_button25);

        button_save.setOnClickListener(this);
        txt01.setOnClickListener(this);
        txt02.setOnClickListener(this);
        txt03.setOnClickListener(this);
        txt04.setOnClickListener(this);
        txt05.setOnClickListener(this);
        txt06.setOnClickListener(this);
        txt07.setOnClickListener(this);
        txt08.setOnClickListener(this);
        txt09.setOnClickListener(this);
        txt10.setOnClickListener(this);
        txt11.setOnClickListener(this);
        txt12.setOnClickListener(this);
        txt13.setOnClickListener(this);
        txt14.setOnClickListener(this);
        txt15.setOnClickListener(this);
        txt16.setOnClickListener(this);
        txt17.setOnClickListener(this);
        txt18.setOnClickListener(this);
        txt19.setOnClickListener(this);
        txt20.setOnClickListener(this);
        txt21.setOnClickListener(this);
        txt22.setOnClickListener(this);
        txt23.setOnClickListener(this);
        txt24.setOnClickListener(this);
        txt25.setOnClickListener(this);

        Cursor cursor = database.rawQuery(LotoTable.SELECT_ALL, null);
        cursor.moveToFirst();

        String str = cursor.getString(LotoTable.COLUMN_LOTO_DRAW_IDX);
        int num = Integer.parseInt(str) + 1;

        draw_number.setText(String.valueOf(num));

        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);

        str = String.format("%02d", day) + "/" + String.format("%02d", month) + "/" + year;

        draw_date.setText(str);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.loto_button1:
                if(txt01.isSelected()){
                    txt01.setSelected(false);
                    list.remove(txt01.getTag().toString());
                }else{
                    txt01.setSelected(true);
                    list.add(txt01.getTag().toString());
                }
                break;
            case R.id.loto_button2:
                if(txt02.isSelected()){
                    txt02.setSelected(false);
                    list.remove(txt02.getTag().toString());
                }else{
                    txt02.setSelected(true);
                    list.add(txt02.getTag().toString());
                }
                break;
            case R.id.loto_button3:
                if(txt03.isSelected()){
                    txt03.setSelected(false);
                    list.remove(txt03.getTag().toString());
                }else{
                    txt03.setSelected(true);
                    list.add(txt03.getTag().toString());
                }
                break;
            case R.id.loto_button4:
                if(txt04.isSelected()){
                    txt04.setSelected(false);
                    list.remove(txt04.getTag().toString());
                }else{
                    txt04.setSelected(true);
                    list.add(txt04.getTag().toString());
                }
                break;
            case R.id.loto_button5:
                if(txt05.isSelected()){
                    txt05.setSelected(false);
                    list.remove(txt05.getTag().toString());
                }else{
                    txt05.setSelected(true);
                    list.add(txt05.getTag().toString());
                }
                break;
            case R.id.loto_button6:
                if(txt06.isSelected()){
                    txt06.setSelected(false);
                    list.remove(txt06.getTag().toString());
                }else{
                    txt06.setSelected(true);
                    list.add(txt06.getTag().toString());
                }
                break;
            case R.id.loto_button7:
                if(txt07.isSelected()){
                    txt07.setSelected(false);
                    list.remove(txt07.getTag().toString());
                }else{
                    txt07.setSelected(true);
                    list.add(txt07.getTag().toString());
                }
                break;
            case R.id.loto_button8:
                if(txt08.isSelected()){
                    txt08.setSelected(false);
                    list.remove(txt08.getTag().toString());
                }else{
                    txt08.setSelected(true);
                    list.add(txt08.getTag().toString());
                }
                break;
            case R.id.loto_button9:
                if(txt09.isSelected()){
                    txt09.setSelected(false);
                    list.remove(txt09.getTag().toString());
                }else{
                    txt09.setSelected(true);
                    list.add(txt09.getTag().toString());
                }
                break;
            case R.id.loto_button10:
                if(txt10.isSelected()){
                    txt10.setSelected(false);
                    list.remove(txt10.getTag().toString());
                }else{
                    txt10.setSelected(true);
                    list.add(txt10.getTag().toString());
                }
                break;
            case R.id.loto_button11:
                if(txt11.isSelected()){
                    txt11.setSelected(false);
                    list.remove(txt11.getTag().toString());
                }else{
                    txt11.setSelected(true);
                    list.add(txt11.getTag().toString());
                }
                break;
            case R.id.loto_button12:
                if(txt12.isSelected()){
                    txt12.setSelected(false);
                    list.remove(txt12.getTag().toString());
                }else{
                    txt12.setSelected(true);
                    list.add(txt12.getTag().toString());
                }
                break;
            case R.id.loto_button13:
                if(txt13.isSelected()){
                    txt13.setSelected(false);
                    list.remove(txt13.getTag().toString());
                }else{
                    txt13.setSelected(true);
                    list.add(txt13.getTag().toString());
                }
                break;
            case R.id.loto_button14:
                if(txt14.isSelected()){
                    txt14.setSelected(false);
                    list.remove(txt14.getTag().toString());
                }else{
                    txt14.setSelected(true);
                    list.add(txt14.getTag().toString());
                }
                break;
            case R.id.loto_button15:
                if(txt15.isSelected()){
                    txt15.setSelected(false);
                    list.remove(txt15.getTag().toString());
                }else{
                    txt15.setSelected(true);
                    list.add(txt15.getTag().toString());
                }
                break;
            case R.id.loto_button16:
                if(txt16.isSelected()){
                    txt16.setSelected(false);
                    list.remove(txt16.getTag().toString());
                }else{
                    txt16.setSelected(true);
                    list.add(txt16.getTag().toString());
                }
                break;
            case R.id.loto_button17:
                if(txt17.isSelected()){
                    txt17.setSelected(false);
                    list.remove(txt17.getTag().toString());
                }else{
                    txt17.setSelected(true);
                    list.add(txt17.getTag().toString());
                }
                break;
            case R.id.loto_button18:
                if(txt18.isSelected()){
                    txt18.setSelected(false);
                    list.remove(txt18.getTag().toString());
                }else{
                    txt18.setSelected(true);
                    list.add(txt18.getTag().toString());
                }
                break;
            case R.id.loto_button19:
                if(txt19.isSelected()){
                    txt19.setSelected(false);
                    list.remove(txt19.getTag().toString());
                }else{
                    txt19.setSelected(true);
                    list.add(txt19.getTag().toString());
                }
                break;
            case R.id.loto_button20:
                if(txt20.isSelected()){
                    txt20.setSelected(false);
                    list.remove(txt20.getTag().toString());
                }else{
                    txt20.setSelected(true);
                    list.add(txt20.getTag().toString());
                }
                break;
            case R.id.loto_button21:
                if(txt21.isSelected()){
                    txt21.setSelected(false);
                    list.remove(txt21.getTag().toString());
                }else{
                    txt21.setSelected(true);
                    list.add(txt21.getTag().toString());
                }
                break;
            case R.id.loto_button22:
                if(txt22.isSelected()){
                    txt22.setSelected(false);
                    list.remove(txt22.getTag().toString());
                }else{
                    txt22.setSelected(true);
                    list.add(txt22.getTag().toString());
                }
                break;
            case R.id.loto_button23:
                if(txt23.isSelected()){
                    txt23.setSelected(false);
                    list.remove(txt23.getTag().toString());
                }else{
                    txt23.setSelected(true);
                    list.add(txt23.getTag().toString());
                }
                break;
            case R.id.loto_button24:
                if(txt24.isSelected()){
                    txt24.setSelected(false);
                    list.remove(txt24.getTag().toString());
                }else{
                    txt24.setSelected(true);
                    list.add(txt24.getTag().toString());
                }
                break;
            case R.id.loto_button25:
                if(txt25.isSelected()){
                    txt25.setSelected(false);
                    list.remove(txt25.getTag().toString());
                }else{
                    txt25.setSelected(true);
                    list.add(txt25.getTag().toString());
                }
                break;
            case R.id.button_save_loto_result:
                if(list.size() != 15){
                    Toast.makeText(this, R.string.str_loto_toast_must_select_15_numbers , Toast.LENGTH_SHORT).show();
                }else if(draw_number.getText().toString() == null) {
                    Toast.makeText(this, R.string.str_loto_toast_draw_number_empty , Toast.LENGTH_SHORT).show();
                }else if(draw_date.getText().toString() == null) {
                    Toast.makeText(this, R.string.str_loto_toast_draw_date_empty , Toast.LENGTH_SHORT).show();
                }else{
                    saveLotoResult();
                    finish();
                }
                break;
        }
    }

    public void saveLotoResult(){
        Collections.sort(list);
        StringBuilder str_result = new StringBuilder();
        ContentValues values = new ContentValues();
        String str;
        String[] array;

        str = draw_number.getText().toString();
        values.put(LotoTable.COLUMN_LOTO_DRAW, str);

        str = draw_date.getText().toString();
        array = str.split("/");
        values.put(LotoTable.COLUMN_LOTO_DAY, array[0]);
        values.put(LotoTable.COLUMN_LOTO_MONTH, array[1]);
        values.put(LotoTable.COLUMN_LOTO_YEAR, array[2]);

        for(int i=0; i<list.size(); i++){
            str_result.append(list.get(i) + "-");
        }

        values.put(LotoTable.COLUMN_LOTO_RESULT, str_result.substring(0, (str_result.length() -1)));

        database.insert(LotoTable.TABLE_LOTOFACIL, null, values);

        Toast.makeText(this, R.string.str_loto_toast_result_added, Toast.LENGTH_SHORT).show();
    }
}
